# Working plan

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con un Launchpad Pro.

La idea es que desde el launchpad puedas cambiar los colores de la tabla, que representan a diferentes actividades.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midiworkingplan/public/

Para usarlo en local:
- npm run build

