
let cellStyle = {
  textAlign: 'center',
  padding: '3px',
  background: 'aliceblue',
  borderRight: '1px solid black',
  borderBottom: '1px dashed black',
  display: 'flex'
}

let fixedLeftStyle = {
  position: 'sticky',
  left: 0,
  display: 'flex',
  background: 'white',
  padding: '5px',
  borderRight: '1px solid black',
  boxShadow: 'rgba(100, 100, 100, 0.13) 4px 3px 6px 0px'
}

let fixedTopStyle = {
  position: 'sticky',
  top: 0,
  zIndex: 199,
  padding: '5px',
  borderBottom: '1px solid black',
  background: 'white',
  display: 'flex',
  paddingTop: '1px',
  paddingBottom: '1px'
}

let fixedTopLeftStyle = {
  top: 0,
  zIndex: 300,
  position: 'sticky',
  left: 0,
  background: 'white',
  padding: '5px',
  borderRight: '1px solid black',
  borderBottom: '1px solid black',
}

let fixedBottomLeftStyle = {
  zIndex: 300,
  position: 'sticky',
  left: 0,
  background: 'white',
  padding: '5px',
  borderRight: '1px solid black',
  borderTop: '1px solid black',
}


let d = new Date(),
    year = d.getYear(),
    days = [[],[],[],[],[],[],[]]

d.setDate(1)
d.setMonth(0)
d.setHours(0)
d.setMinutes(0)
d.setSeconds(0)
d.setMilliseconds(0)

while (d.getYear() === year) {
  let pushDate = {date:new Date(d.getTime())}
  let day = pushDate.date.getDay()-1
  if (day === -1) day = 6
  days[day].push({style: cellStyle, data:pushDate})
  d.setDate(d.getDate() + 1)
}

let names = days[0].filter((data, index)=>!(index%2))
    .map(date => { return { style: fixedTopStyle, data: date.data.date.toLocaleDateString('es-ES', {month: 'short'}).toUpperCase() } } )

days = [
  days[0].filter((data, index)=>!(index%2)),
  days[1].filter((data, index)=>!(index%2)),
  days[2].filter((data, index)=>!(index%2)),
  days[3].filter((data, index)=>!(index%2)),
  days[4].filter((data, index)=>!(index%2)),
  days[5].filter((data, index)=>!(index%2)),
  days[6].filter((data, index)=>!(index%2)),
  days[0].filter((data, index)=>(index%2)),
  days[1].filter((data, index)=>(index%2)),
  days[2].filter((data, index)=>(index%2)),
  days[3].filter((data, index)=>(index%2)),
  days[4].filter((data, index)=>(index%2)),
  days[5].filter((data, index)=>(index%2)),
  days[6].filter((data, index)=>(index%2)),
]

let max = Math.max(...days.map(days => days.length))
let daysMax = days.map(days => (days.length<max)?days.concat({style: cellStyle, data:''}):days)

export default [
  [{style: fixedTopLeftStyle, data: ''}].concat(names),
  [{style: fixedLeftStyle, data: 'L'}].concat(daysMax[0]),
  [{style: fixedLeftStyle, data: 'M'}].concat(daysMax[1]),
  [{style: fixedLeftStyle, data: 'X'}].concat(daysMax[2]),
  [{style: fixedLeftStyle, data: 'J'}].concat(daysMax[3]),
  [{style: fixedLeftStyle, data: 'V'}].concat(daysMax[4]),
  [{style: fixedLeftStyle, data: 'S'}].concat(daysMax[5]),
  [{style: fixedLeftStyle, data: 'D'}].concat(daysMax[6]),
  [{style: fixedLeftStyle, data: 'L'}].concat(daysMax[7]),
  [{style: fixedLeftStyle, data: 'M'}].concat(daysMax[8]),
  [{style: fixedLeftStyle, data: 'X'}].concat(daysMax[9]),
  [{style: fixedLeftStyle, data: 'J'}].concat(daysMax[10]),
  [{style: fixedLeftStyle, data: 'V'}].concat(daysMax[11]),
  [{style: fixedLeftStyle, data: 'S'}].concat(daysMax[12]),
  [{style: fixedLeftStyle, data: 'D'}].concat(daysMax[13]),

    ]
