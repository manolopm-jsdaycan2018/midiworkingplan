'use strict'

import React from 'react'
import ReactDOM from 'react-dom'

export default class Table extends React.Component {
  constructor(props) {
    super(props)
    let columnsCount = props.data ? props.data[0].length : 0
    let col = new Array(columnsCount).fill('N')

    this.state = {
      scrollTop: 0,
      scrollLeft: 0,
      col: col
    }

    this.getMidiMessage = this.getMidiMessage.bind(this)
  }

  getMidiMessage (midiMessage) {
    let top = 0;
    let left = 0;
    const distance = 123.9
    const node = ReactDOM.findDOMNode(this)
    let st = this.state.col
    
    switch ( midiMessage.data[0] ) {
    case 176: //control
      if (midiMessage.data[2]>0){
        switch ( midiMessage.data[1] ) {
        case 91: //L
          if (this.state.scrollLeft>0)
            left = -distance
          break
        case 92: //R
          if (this.state.scrollLeft<node.scrollWidth)
            left = distance
          break
        case 93: //D
          if (this.state.scrollTop<node.scrollHeight)
            top = distance
          break
        case 94: //U
          if (this.state.scrollTop>0)
            top = -distance
          break
        default: {
          let col = 8-Math.floor(midiMessage.data[1]/10)
          let rot = ['N','V','R','L']
          st[col] = rot[(rot.indexOf(st[col])+1)%4]          
          break
        }
        }
        this.setState({
          scrollTop: this.state.scrollTop + top,
          scrollLeft: this.state.scrollLeft + left,
          col: st
        })

      }
      break
    }
  }
  

  componentDidMount () {

    navigator.requestMIDIAccess().then(
      midi => {
        let deviceIdIn = ''
        let deviceIdOut = ''
        midi.inputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdIn = device.id
        })

        midi.outputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdOut = device.id
        })

        this.setState(Object.assign(
          this.state,
          {
            input: midi.inputs.get(deviceIdIn),
            output: midi.outputs.get(deviceIdOut)
          }))

        midi.inputs.get(deviceIdIn).onmidimessage = this.getMidiMessage
        
        for (let i = 0; i < 100; i++) {
          midi.outputs.get(deviceIdOut).send([0x80, i, 0])
        }
      },
      error => {
        console.log("MIDI ERROR:", error)
        this.setState(Object.assign(this.state, {input: null, output: null}))
      }
    )

    
    const node = ReactDOM.findDOMNode(this)

    this.setState({
      scrollTop: node.scrollTop,
      scrollLeft: node.scrollLeft
    })
    
  }

  componentDidUpdate (prevProps) {

    const node = ReactDOM.findDOMNode(this)
    node.scrollTop = this.state.scrollTop
    node.scrollLeft = this.state.scrollLeft
  }
  
  render () {
    let data = this.props.data ? this.props.data : []
    let rowsCount = data.length 
    let columnsCount = data[0] ? this.props.data[0].length : 0

    let style = {
      display: 'grid',
      gridTemplateRows: 'auto '.repeat(rowsCount).slice(0, -1),
      gridTemplateColumns: 'auto '.repeat(columnsCount).slice(0, -1),
      overflow: 'scroll',
      height: 'inherit',
      width: '400px'
      
    }
    
    let divs = data.map((row, i) => {
      return data[i].map((column, j) => {
        let tmpStyle = Object.assign({}, this.props.innerStyle)
        let  dataStyle = Object.assign(tmpStyle, column.style)

        let box = null
        if (dataStyle.background==='aliceblue'){
          let color = ''
          let text = ''
          let lightColor = 0


          switch (this.state.col[j-1]) {
          case 'N':
            color = 'rgb(241,203,89)'
            text = 'TRABAJA'
            lightColor = 96
            break
          case 'V':
            color = 'rgb(89, 241, 167)'
            text = 'DESCANSA'
            lightColor = 22
            break
          case 'R':
            color = 'rgb(241, 89, 138)'
            text = 'VACACIONES'
            lightColor = 95
            break
          case 'L':
            color = 'rgb(130, 130, 130)'
            text = 'REEMPLAZO'
            lightColor = 2
            break
          }

          if ((i<8 && j<9) && (this.state.output != null) )
            this.state.output.send([144, 88-(i-1)-(j-1)*10, lightColor]); 

            box = <div style={{display:'grid',minWidth:'100px',background:color,border:'1px solid grey',gridTemplateRows:'auto max-content auto',gridTemplateColumns:'auto max-content auto'}}>
          <div style={{fontSize:'9px',gridRow:'1',gridColumn:'1'}}></div>
          <div style={{fontSize:'9px',gridRow:'1',gridColumn:'3'}}></div>
          <div style={{fontSize:'12px',gridRow:'2',gridColumn:'1 / span 3'}}>{text}</div>
          <div style={{fontSize:'9px',gridRow:'3',gridColumn:'1'}}></div>
          <div style={{fontSize:'9px',gridRow:'3',gridColumn:'2'}}></div>
          <div style={{fontSize:'9px',gridRow:'3',gridColumn:'3'}}></div>
          </div>
        }
        let textStyle = dataStyle.background==='aliceblue'?{fontSize:'10px',textAlign:'right',paddingLeft:'3px',display:'flex',flex:'1',alignSelf:'flex-end',justifyContent:'flex-end'}:
            {display:'flex',flex:'1',justifyContent:'center',alignContent:'center',alignSelf:'center'}

        if (dataStyle.background === 'aliceblue')
          dataStyle.background = (typeof(column.data)!=='string'&&column.data.date.getMonth()%2)?'aliceblue':'#d1eaff'


        
        return (
            <div key={'table'+i+'.'+j} style={dataStyle}>
            {box}
            <div style={textStyle}>
            {typeof(column.data)!=='string'?column.data.date.getDate():column.data}
            </div>
            </div>
        )
      })
    })

    return (
        <div style={style}>
        { divs }
      </div>
    )
  }
}
