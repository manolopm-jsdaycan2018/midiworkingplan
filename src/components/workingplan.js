'use strict'

import React from 'react'
import Table from './table'
import Data from './data'

export default class Workingplan extends React.PureComponent {
  render () {
    const innerStyle = {
      minWidth: 'max-content',
      background:'grey'
    }
    
    return (
        <Table innerStyle={innerStyle} data={Data}>
        </Table>
    )

  }
}

