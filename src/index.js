import React from 'react'
import ReactDOM from 'react-dom'
import Workingplan from './components/workingplan.js'

ReactDOM.render(<Workingplan />, document.getElementById('content'))
